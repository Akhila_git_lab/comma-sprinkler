Python 3.9.1 (tags/v3.9.1:1e5d33e, Dec  7 2020, 17:08:21) [MSC v.1927 64 bit (AMD64)] on win32
Type "help", "copyright", "credits" or "license()" for more information.
>>> def words_preceding_comma(string):
    comma_seperated_list = string.split(",")
    space_seperated_list = []
    for word in comma_seperated_list:
      space_seperated_list.append(word.split(" "))
    list_of_words_preceding_commas = []
    for xy in space_seperated_list:
        if xy[-1][-1] != '.':
          list_of_words_preceding_commas.append(xy[-1])
    return list(set(list_of_words_preceding_commas))

def adding_preceding_commas(string):
  list1 = words_preceding_comma(string)
  list2 = string.split()
  list2 = [item + ','  if item in list1 else item for item in list2] 
  string1 = ""
  for i in list2:
    string1 = string1 + " " + i
  return string1

def words_succeeding_comma(string):
    comma_seperated_list = string.split(",")
    space_seperated_list = []
    for word in comma_seperated_list:
      space_seperated_list.append(word.split(" "))
    list_of_words_succeeding_commas = []
    for xy in space_seperated_list:
      if xy != space_seperated_list[0]:
        list_of_words_succeeding_commas.append(xy[1])
    list_of_words_succeeding_commas = [s.replace(".", "") for s in list_of_words_succeeding_commas]
    return list(set(list_of_words_succeeding_commas))

def adding_succeeding_commas(string):
  list1 = words_succeeding_comma(string)
  list2 = string.split()
  list2 = [', ' + item if item.replace(".", "").replace(",", "") in list1 and item != list2[0] else item for item in list2] 
  string1 = ""
  for i in list2:
      if i[0] == ',':
        string1 = string1 + i
      else:
        string1 = string1 + " "  + i
  string1 = string1.replace(".,", ".").replace(",,", ",")
  return string1

def combined_string(string):
  return adding_succeeding_commas(adding_preceding_commas(string))[1:]

def required_sentence(string):
  i = 0
  for i in range(len(string.split())):
    string = combined_string(string)
    i += 1
  return string

input_string= input()
print(required_sentence(input_string))