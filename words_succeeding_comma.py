Python 3.9.1 (tags/v3.9.1:1e5d33e, Dec  7 2020, 17:08:21) [MSC v.1927 64 bit (AMD64)] on win32
Type "help", "copyright", "credits" or "license()" for more information.
>>> def words_succeeding_comma(string):
    comma_seperated_list = string.split(",")
    space_seperated_list = []
    for word in comma_seperated_list:
      space_seperated_list.append(word.split(" "))
    list_of_words_succeeding_commas = []
    for xy in space_seperated_list:
      if xy != space_seperated_list[0]:
        list_of_words_succeeding_commas.append(xy[1])
    list_of_words_succeeding_commas = [s.replace(".", "") for s in list_of_words_succeeding_commas]
    return list(set(list_of_words_succeeding_commas))