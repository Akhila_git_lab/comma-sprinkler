
def adding_preceding_commas(string):
  list1 = words_preceding_comma(string)
  list2 = string.split()
  list2 = [item + ','  if item in list1 else item for item in list2]
  string1 = ""
  for i in list2:
    string1 = string1 + " " + i
  return string1
