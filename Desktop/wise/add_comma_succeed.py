def add_comma_succeedded(string):
  list1 = succeeded_word(string)
  list2 = string.split()
  list2 = [', ' + item if item.replace(".", "").replace(",", "") in list1 and item != list2[0] else item for item in list2]
  string1 = ""
  for i in list2:
      if i[0] == ',':
        string1 = string1 + i
      else:
        string1 = string1 + " "  + i
  string1 = string1.replace(".,", ".").replace(",,", ",")
  return string1
