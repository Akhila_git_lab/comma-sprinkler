def adding_succeeding_commas(string):
  list1 = words_succeeding_comma(string)
  list2 = string.split()
  list2 = [', ' + item if item.replace(".", "").replace(",", "") in list1 and item != list2[0] else item for item in list2] 
  string1 = ""
  for i in list2:
      if i[0] == ',':
        string1 = string1 + i
      else:
        string1 = string1 + " "  + i
  string1 = string1.replace(".,", ".").replace(",,", ",")
  return string1
